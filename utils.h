#ifndef UTILS_H
#define UTILS_H

#include <QThread>

class Utils : public QThread
{
    Q_OBJECT
public:
    static void msleep(unsigned long ms);
    static void usleep(unsigned long us);
    static void sleep(unsigned long s);
};

#endif // UTILS_H
