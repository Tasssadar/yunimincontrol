#ifndef YUNIMINCOMMUNICATION_HPP
#define YUNIMINCOMMUNICATION_HPP

#include <QThread>
#include <iostream>
#include <string>
#include <time.h>
//#include <conio.h>
#include "connectionOverTCP.hpp"
#include "convert.hpp"

#include "fake_conio.h"
#include "utils.h"

int g_req_speed_left = 0;
int g_req_speed_right = 0;
bool g_led_status = false;

void doNothing()
{
}

void setLeftMotor(int leftMotor)
{
    sendMessageToServer("setLeftMotor", IntToStr(leftMotor) );
    g_req_speed_left = leftMotor;
}

void setRightMotor(int rightMotor)
{
    sendMessageToServer("setRightMotor", IntToStr(rightMotor));
    g_req_speed_right = rightMotor;
}

void stopLeftMotor()
{
    setLeftMotor(0);
}

void stopRightMotor()
{
    setRightMotor(0);
}

void setMotorPower(int leftMotor, int rightMotor)
{
    setLeftMotor(leftMotor);
    setRightMotor(rightMotor);
}

int getSensorValue(int sensor)
{
    return StrToInt( sendMessageToServer("getSensorValue",IntToStr(sensor)) );
}

void wait(unsigned int microseconds)
{
    Utils::usleep ( microseconds );
}

class stopwatch
{
private:
    int startTime;
    int stopTime;
    bool run;
public:
    stopwatch()
    {
        startTime = clock();
        run = true;
    }
    void start()
    {
        if(!run)
            startTime = clock();
        run = true;
    }
    void stop()
    {
        if(run)
        {
            stopTime = clock() - startTime;
            run = false;
        }
    }
    void clear()
    {
        startTime = clock();
    }
    int getTime()
    {
        if(run)
            return (clock()-startTime)*(1000000/CLOCKS_PER_SEC);
        else
            return stopTime*(1000000/CLOCKS_PER_SEC);
    }
};

void waitForStartButton()
{
    while( sendMessageToServer("startButton","get") == "0" )
    {
        doNothing();
    }
}

bool isStartButtonPressed()
{
    if( sendMessageToServer("startButton","get") == "0" )
        return false;
    else
        return true;
}

void setLeftServo(int value)
{
    sendMessageToServer("leftServoValue",IntToStr(value));
}

void setRightServo(int value)
{
    sendMessageToServer("rightServoValue",IntToStr(value));
}

void setLed()
{
    sendMessageToServer("led","1");
    g_led_status = true;
}

void clearLed()
{
    sendMessageToServer("led","0");
    g_led_status = false;
}

void toggleLed()
{
    if(g_led_status)
        clearLed();
    else
        setLed();
}

class Encoder
{
private:
    int m_value;
    bool m_isRunning;

protected:
    std::string m_valueOnServer;
    void setLeftOrRightEncoder(){ m_valueOnServer = "encoderValue"; }

public:
    Encoder()
    {
        setLeftOrRightEncoder();
        if(gConnection.isReady())
            m_value = StrToInt( sendMessageToServer(m_valueOnServer,"get") );
        else
            m_value = 0;
        m_isRunning = true;
    }
    void start()
    {
        if(!m_isRunning)
            m_value = StrToInt( sendMessageToServer(m_valueOnServer,"get") );
        m_isRunning = true;
    }
    void stop()
    {
        if(m_isRunning)
        {
            m_value = StrToInt( sendMessageToServer(m_valueOnServer,"get") ) - m_value;
            m_isRunning = false;
        }
    }
    void clear()
    {
        m_value = StrToInt( sendMessageToServer(m_valueOnServer,"get") );
    }
    int get()
    {
        if(m_isRunning)
            return StrToInt( sendMessageToServer(m_valueOnServer,"get") ) - m_value;
        else
            return m_value;
    }
};

class left_encoder : public Encoder
{
    virtual void setLeftOrRightEncoder(){ m_valueOnServer = "leftEncoderValue"; }
};

class right_encoder : public Encoder
{
    virtual void setLeftOrRightEncoder(){ m_valueOnServer = "rightEncoderValue"; }
};

class rs232_t
{
    bool enableTCP;

    void sendStringToTCP(std::string text)
    {
        //
    }

public:
    rs232_t()
    {
        enableTCP = false;
    }

    ~rs232_t()
    {
        //
    }

    bool setTCPConnection(std::string ip, int port)
    {
        //
    }

    void send(std::string const & text)
    {
        std::cout << text;
        if(enableTCP)
        {
            sendStringToTCP(text);
        }
    }
    void sendCharacter(char pismeno)
    {
        //sendMessageToServer("rs232",CharToString(pismeno));
        std::cout << pismeno;

        std::string toSend;
        toSend.push_back(pismeno);
        sendStringToTCP(toSend);
    }
    void sendNumber(int number,int lengh = 0)
    {
        std::string buffer = "";
        int mezer = lengh - IntToStr(number).size();
        for(int i=0;i<mezer;++i)
        {
            buffer += ' ';
        }
        buffer += IntToStr(number);
        //sendMessageToServer("rs232",buffer);
        std::cout << buffer;

        sendStringToTCP(buffer);
    }
    char get()
    {
        /*std::string pomoc = "";
        while( (pomoc = sendMessageToServer("rs232get","")) == "" )
        {
            doNothing();
        }
        return pomoc[0];*/
        return _getch();
    }
    bool peek(char & ch)
    {
        /*std::string pomoc = sendMessageToServer("rs232get","");
        if( pomoc != "" )
        {
            ch = pomoc[0];
            return true;
        }
        else
        {
            return false;
        }*/
        if(_kbhit())
        {
            ch = _getch();
            return true;
        }
        else
        {
            return false;
        }
    }
    void wait()
    {
    }
};
rs232_t rs232;

//i2c a dalsi veci, ktere nejsou v referencni prirucce ted neresim//

//implementace imaginarnich funkci

void setRColor(int red)
{
    sendMessageToServer("Rcolor",IntToStr(red));
}

void setGColor(int green)
{
    sendMessageToServer("Gcolor",IntToStr(green));
}

void setBColor(int blue)
{
    sendMessageToServer("Bcolor",IntToStr(blue));
}

void setRGBColor(int R, int G, int B)
{
    setRColor(R);
    setGColor(G);
    setBColor(B);
}

int getNumberOfRobots()
{
    return StrToInt( sendMessageToServer("numberOfRobots","get") );
}

float getDistanceFromRobot(int idRobota)
{
    return (float)StrToFloat( sendMessageToServer("distanceFromRobot", IntToStr(idRobota) ) );
}

float getAngleFromRobot(int idRobota)
{
    return (float)StrToFloat( sendMessageToServer("angleFromRobot", IntToStr(idRobota) ) );
}

int getMyId()
{
    return StrToInt( sendMessageToServer("getMyId","") );
}

bool amIAlive()
{
    if( sendMessageToServer("amIAlive","") == "1" )
        return true;
    else
        return false;
}

bool setPosition(float X, float Y)
{
    sendMessageToServer("positionX",FloatToStr(X));
    sendMessageToServer("positionY",FloatToStr(Y));
    return true;
}

bool setRotation(float rot)
{
    sendMessageToServer("rotationZ",FloatToStr(rot));
    return true;
}

#endif // YUNIMINCOMMUNICATION_HPP
