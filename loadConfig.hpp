#ifndef LOADCONFIG_HPP
#define LOADCONFIG_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "convert.hpp"

class globalConfiguration
{
public:
    std::map<std::string,std::string> data;

    bool loadFile(std::string const & fileName)
    {
        std::cout << "Loading configuration file \"" << fileName << "\" ..." << std::endl;
        std::ifstream ifs(fileName.c_str());
        bool fileLoaded = false;

        fileLoaded = true;
        if (!ifs)
        {
            std::cout << "Nepodarilo se otevrit konfiguracni soubor " << fileName << std::endl;
            fileLoaded = false;
        }

        std::string line;
        while (getline(ifs, line))
        {
            int index = 0;
            for(index = 0; line[index] != ' '; index++);
            std::string id;
            id.append(line.begin(), line.begin()+index);

            for(index++; line[index] != ' '; index++);
            std::string value;
            value.append(line.begin()+index+1, line.end());

            data[id] = value;
        }

        ifs.close();
        return fileLoaded;
    }

    globalConfiguration()
    {
    }

    globalConfiguration(std::string const & fileName)
    {
        loadFile(fileName);
    }

    std::string getValue(std::string id)
    {
        return data[id];
    }

    int getIntValue(std::string id)
    {
        return StrToInt(data[id]);
    }

    float getFloatValue(std::string id)
    {
        return (float)StrToFloat(data[id]);
    }
};

#endif // LOADCONFIG_HPP
