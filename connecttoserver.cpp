#include <QCoreApplication>

#include "connecttoserver.h"
#include "utils.h"

ConnectToServer::ConnectToServer(QObject *parent) :
    QObject(parent)
{
}

ConnectToServer::ConnectToServer(std::string ip, int port, QObject *parent) : QObject(parent)
{
    start(ip, port);
}

bool ConnectToServer::isReady()
{
    return m_socket.isOpen();
}

bool ConnectToServer::start(std::string const & ip, int port)
{
    connect(&m_socket, SIGNAL(connected()), this, SLOT(startTransfer()));
    m_socket.connectToHost(QHostAddress(ip.c_str()), port);
    return m_socket.isOpen();
}

void ConnectToServer::close()
{
    m_socket.close();
}

std::string ConnectToServer::sendStringToServer(std::string const & value)
{
    m_socket.write(value.c_str());

    while(!m_socket.canReadLine())
    {
        QCoreApplication::processEvents();
        Utils::msleep(50);
    }
    return std::string(m_socket.readLine().data());
}

void ConnectToServer::startTransfer()
{
    qDebug("transfer");
    for(int i=0;i<1000;i++)
    {
        m_socket.write("ahoj\n");
        m_socket.flush();
    }
}
