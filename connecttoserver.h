#ifndef CONNECTTOSERVER_H
#define CONNECTTOSERVER_H

#include <QObject>

#include <iostream>
#include <string>
#include <QTcpSocket>
#include <QHostAddress>
#include <QObject>

#define BUFSIZE 4096

class ConnectToServer : public QObject
{
    Q_OBJECT
public:
    explicit ConnectToServer(QObject *parent = 0);
    ConnectToServer(std::string ip, int port, QObject *parent = 0);

    bool isReady();
    bool start(std::string const & ip, int port);
    void close();
    std::string sendStringToServer(std::string const & value);

private slots:
    void startTransfer();

private:
    QTcpSocket m_socket;
    bool closeError;
};

#endif // CONNECTTOSERVER_H
