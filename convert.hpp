#ifndef CONVERT_HPP
#define CONVERT_HPP

#include <string>
#include <sstream>

std::string IntToStr(int a)
{
    std::ostringstream pomoc;
    pomoc << a;
    return pomoc.str();
}

int StrToInt(std::string text)
{
    if(text == "")
    {
        return 0;
    }
    return atoi(text.c_str());
}

double StrToFloat(std::string text)
{
    if(text == "")
    {
        return 0;
    }
    return strtod(text.c_str(),NULL);
}

std::string FloatToStr(double a)
{
    std::ostringstream pomoc;
    pomoc << a;
    return pomoc.str();
}

std::string CharToString(char a)
{
    std::string s(1, a);
    return s;
}

double modulo(double a, double b)
{
    double c = a/b;
    c = c - (int)c;
    return c * b;
}

#endif // CONVERT_HPP
