#ifndef YUNIMIN3_H
#define YUNIMIN3_H

#include <QCoreApplication>

#include "YuniminCommunication.hpp"
#include "loadConfig.hpp"

#include "utils.h"

bool initialization()
{
    setConnection();
    return startClient();
}

void run();

void finish()
{
    endClient();
}

int singleRobot(void)
{
    if( !initialization() )
    {
        return 0;
    }
    std::cout << "Program is running..." << std::endl;
    run();
    std::cout << "Program finished..." << std::endl;
    finish();
    return 0;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ///<begin> Testing TCP connection class.
    ConnectToServer t("127.0.0.1", 12345);
    for(int i = 0; i < 5000; i += 50)
    {
        QCoreApplication::processEvents();
        Utils::msleep(50);
    }

    qDebug("received line: %s", t.sendStringToServer("odpovez!").c_str());

    t.close();
    ///<end> Testing TCP connection class.

    singleRobot();
    return a.exec();
}

#endif // YUNIMIN3_H
