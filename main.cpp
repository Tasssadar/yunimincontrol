#include "Yunimin3.h"

void setConnection()
{
    globalConfiguration config;
    gRobotName = "Yunimin";
    config.loadFile(gRobotName + ".ini");
    gConnectionAddress = config.getValue("ip");
    gConnectionPort = config.getIntValue("port");
    //rs232.setTCPConnection("localhost", 5863);
}

int odectiKNule(int value, int mensitel)
{
    if(value>0)
        return value - mensitel;
    else if(value<0)
        return value + mensitel;
    else
        return 0;
}

void run()
{
    char const up = 'w';
    char const down = 's';
    char const left = 'a';
    char const right = 'd';
    char const reset = ' ';
    char const sensor = 'x';

    int const power = 60;
    int const minPower = 30;

    int leftPower = 0;
    int rightPower = 0;

    bool search = false;

    left_encoder le;
    le.start();

    rs232.send("Serial output:\n\n");
    for(;;)
    {
        rs232.sendNumber(getSensorValue(1));
        rs232.send("   ");
        rs232.sendNumber(le.get());
        rs232.send("                 \r");
        char ch = 0;
        if(rs232.peek(ch))
        {
            if(ch == up)
            {
                leftPower = power;
                rightPower = power;
            }
            if(ch == down)
            {
                leftPower = -power;
                rightPower = -power;
            }
            if(ch == left)
            {
                leftPower = odectiKNule(leftPower, minPower);
            }
            if(ch == right)
            {
                rightPower = odectiKNule(rightPower, minPower);
            }
            if(ch == reset)
            {
                leftPower = 0;
                rightPower = 0;
            }
            if(ch == sensor)
            {
                if(search)
                    search = false;
                else
                    search = true;
            }
        }

        if(search)
        {
            if(getSensorValue(1) >= 127)
                setMotorPower(0,30);
            else
                setMotorPower(30,30);
        }
        else
        {
            setMotorPower(leftPower, rightPower);
            wait(100000);
        }
    }
}
