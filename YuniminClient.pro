#-------------------------------------------------
#
# Project created by QtCreator 2013-12-13T19:03:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

QT += network

TARGET = YuniminClient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    utils.cpp \
    connecttoserver.cpp

HEADERS += \
    Yunimin3.h \
    YuniminCommunication.hpp \
    convert.hpp \
    connectionOverTCP.hpp \
    loadConfig.hpp \
    fake_conio.h \
    utils.h \
    connecttoserver.h
