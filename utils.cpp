#include "utils.h"

void Utils::msleep(unsigned long ms)
{
    QThread::msleep(ms);
}

void Utils::usleep(unsigned long us)
{
    QThread::usleep(us);
}

void Utils::sleep(unsigned long s)
{
    QThread::sleep(s);
}
