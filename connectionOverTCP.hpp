#ifndef CONNECTIONOVERTCP_HPP
#define CONNECTIONOVERTCP_HPP

#include "connecttoserver.h"

std::string gRobotName = "";
std::string gConnectionAddress = "";
int gConnectionPort = 0;

void setConnection();

ConnectToServer gConnection;

///<begin> old code, do not run!
std::string sendStringToServer(std::string const & value)
{
    return "";
}

std::string sendMessageToServer(std::string const & type, std::string const & value)
{
    std::string text = type + "\n" + value;
    text = sendStringToServer(text);
    if(text == "error")
    {
        //closeError = true;
    }
    return text;
}

bool startClient()
{
    //

    while( sendMessageToServer("connect",gRobotName) == "0" )
    {
        std::cout << "K serveru je jiz pripojen stejnojmenny robot." << std::endl << "Zadejte jine jmeno robota:" << std::endl;
        getline( std::cin, gRobotName, '\n' );
    }
    std::cout << "Connected to server..." << std::endl;
    return true;
}

bool endClient()
{
    sendMessageToServer("finish","");
    //
    std::cout << "Disconnected from server..." << std::endl;
    return true;
}
///<end> old code

#endif // CONNECTIONOVERTCP_HPP
